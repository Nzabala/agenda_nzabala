<?php
session_start();
if($_SESSION['ID_USER'] == null) {
        	header("location: home.php");
}
include "conexion.php";
$con = mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_NAME);
if (mysqli_error($con)){
	echo "<p>Error: ".mysqli_error($con).".</p>";
}
?>
<script type='text/javascript'>
			// ocultar el iframe si clican al formulario con enviarFiltro			
			var resultado;
			resultado="<?php echo $_POST['enviarFiltro']; ?>";		  
		   if (resultado != ""){
				parent.location.href="agenda.php?filtro=true&campo="+"<?php echo $_POST['campo'];?>"+"&orden="+"<?php echo $_POST['orden'];?>";
		   }
</script>
<html>
<head>
        <title>Agenda</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./css/filter.css">
        <script type='text/javascript' src='./js/funciones.js'></script>
</head>
<body>
<div id="content">
	<div id="secondHeader"><span>Filtrar Contactos</span></div>
	<div id="contactInfo">
		<form action="" method="post"><table><tr>
			<td><select name="campo">
   			<option value="NAME">Nombre</option>
    			<option value="SURNAME">Apellido</option>
    			<option value="TELF">telefono</option>
    			<option value="BIRTHDATE">Fecha Nacimiento</option>
  			</select></td>
			<td><input type="radio" name="orden" value="ASC"></td><td>Asc</td><td><input type="radio" name="orden" value="DES"></td><td>Desc</td><td><input type="submit" name="enviarFiltro" value="Filtrar" onclick="ocultarPromtF()"/></td>
		</tr></table></form>
	</div>
</div>
</body>
</html>