<!-- Footer used in every page -->
<link rel="stylesheet" type="text/css" href="./css/footer.css" />
<footer>
    <div id = "footercontacts">
        <img src="./img/icons/phone.png" class="contactsImage"/> +34 666 12 34 (Spain)
        <img src="./img/icons/email.png" class="contactsImage"/>  a15niczabig@iam.cat
        <img src="./img/icons/address.png" class="contactsImage"/> Crta Esplugues 56 08860 Barcelona
    </div>

    <div id="copyright">Copyright © 2016 DAW Nico </div>

</footer>