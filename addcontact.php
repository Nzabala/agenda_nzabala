<html>
    <head>
        <title>Agenda</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./css/addcontact.css">
        <script type='text/javascript' src='./js/calendario.js'></script>
        <script type='text/javascript' src='./js/funciones.js'></script>
		  <?php 
		  
        session_start();
        if($_SESSION['ID_USER'] == null) {
        	header("location: home.php");
        }
		  include "conexion.php";
		  $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_NAME);
		  if (mysqli_error($con)){
				echo "<p>Error: ".mysqli_error($con).".</p>";
		  }
		  if(isset($_POST['submit'])){
				$sql = "INSERT INTO CONTACT(ID_USER_FK,NAME,SURNAME,TELF,BIRTHDATE) VALUES('".$_SESSION['ID_USER']."','".$_POST['nombre']."','".$_POST['apellidos']."','".$_POST['telefono']."','".$_POST['fec_nac']."')";			
				$result = mysqli_query($con, $sql);
				header("location:agenda.php");
		  }
        ?>
    </head>
    <body onload="ocultarcalendario()">
        <?php include("./header.php"); ?>
        <div id="content">
            <div id="secondHeader">
                <h2>Añadir nuevo contacto</h2>
            </div>
            <form action="" method="post" onsubmit="return validarForm()" name="addcontact">
            <div id="contactInfo">
						<img src="./img/icons/nombre.png" alt=""><input type="text" name="nombre" placeholder="nombre"><input type="text" name="apellidos" placeholder="apellidos"><div class="enter"></div>
						<img src="./img/icons/telefono.png" alt=""><input type="text" name="telefono" placeholder="telefono"><div class="enter"></div>
						<img src="./img/icons/fec_nac.png" alt=""><input onClick="mostrarcalendario()" type="text" name="fec_nac" placeholder="fecha nacimiento"><div class="enter"></div>    	
						<iframe src="calendario.html" id="calendar" width="680px" height="220px" scrolling="no"></iframe>
            	
            </div> 
            <div id="contactButton">
                <input class="return" Type="button" onClick="history.go(-1);return true;" value="Volver">
                <input class="guardar" type="submit" name="submit" value="Guardar">
            </div> 
            </form>
        </div>
        <div class="enter"></div>
        <?php include("./footer.php"); ?>
    </body>
</html>
