<?php
session_start();

include "conexion.php";

//Esta funcion la llamará en el momento que clickemos en el botón de login
function verificar_login($user,$pass){
	//conectará a la base de datos que tiene las constantes en el include del conexion.php.
	$con = mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_NAME);
	$sql = "SELECT * FROM USER WHERE USERNAME = '$user' AND PASS = '$pass'";
	$result = mysqli_query($con, $sql);

	if (mysqli_error($con)){
		echo "<p>Error: ". mysqli_error($con). ".</p>";
	}
	//si hay resultados, (mas de una linea de consulta sql), en la variable $row guardará en forma de array cada campo con su nombre
	if (mysqli_num_rows($result) > 0) {
		while($row = mysqli_fetch_assoc($result)) {
			$_SESSION['ID_USER'] = $row["ID_USER"];
			$_SESSION['USER'] = $row["USERNAME"];
			
			return true;
		}
	}else {
		echo "Usuario o Contraseña erroneo";
		return false;
	}
}
if(!isset($_SESSION['userid'])){
	if(isset($_POST['login'])){
		if(verificar_login($_POST["user"],$_POST["password"])){
			header("location:agenda.php");		
		}
	}
}


?>
<!-- UN HOME la clase enter es general en el css para limpiar todo tipo de float que haya antes del div. -->
<html>
    <head>
        <title>Agenda</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./css/home.css">
    </head>
    <body>
        <div id="header">
        		<img class="slide" src="./img/header_Home_cut.jpg" alt="Agenda Online">
        		<img class="logo" src="./img/Logo.png" alt="Nic" width="100" height="100">
					<div class="login">            		
            		<h1 class="home">Agenda Online</h1>
            			<form action="" method="post" class="login">
            				<div class="usrpass">
            					<input type="text" name="user" placeholder="usuario"><br>
            					<input type="password" name="password" placeholder="contraseña"><br>
            				</div>
                            <input name="login" type="submit" value="login">
            			</form>
            	</div>
            	<div class="enter"></div>
        </div>
        <div class="enter"></div>
        <div id="info">
        		<h2 class="home">Descubre lo que Agenda Online puede hacer por ti</h2>
        		<div class="two-colums">
        		<img src="./img/Agenda_online.png" alt="">
        		<ul>
					<li>Gestiona facilmente tus contactos en cualquier lado.</li>
					<li>Administra y organiza tus contactos cómodamente.</li>
					<li>Búsqueda fácil de contactos por campos.</li>
					<li>¿Te has quedado sin batería en el móvil? <br> Agenda Online es tu mejor opción!</li>        		
        		</ul>
        		<div class="enter"></div>
        		</div>        
        </div>
        <?php include("./footer.php"); ?>
    </body>
</html>
