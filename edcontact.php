<html>
    <head>
        <title>Agenda</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./css/addcontact.css">
        <script type='text/javascript' src='./js/calendario.js'></script>
        <script type='text/javascript' src='./js/funciones.js'></script>
		  <?php 
        session_start();
		  include "conexion.php";
			if($_SESSION['ID_USER'] == null) {
        	header("location: home.php");
        }
		  $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_NAME);
		  if (mysqli_error($con)){
				echo "<p>Error: ".mysqli_error($con).".</p>";
		  }
		  if(isset($_POST['submit'])){
		  		$contactId = $_GET['id'];
		  		$nombre = $_POST['nombre'];
		  		$apellidos = $_POST['apellidos'];
		  		$telefono = $_POST['telefono'];
		  		$fec_nac = $_POST['fec_nac'];
		  		
				$sql = "UPDATE CONTACT SET NAME = '$nombre', SURNAME = '$apellidos', TELF = '$telefono',BIRTHDATE = '$fec_nac' WHERE ID_CONTACT = '$userid'";			
				$result = mysqli_query($con, $sql);
				header('location: ./agenda.php?ref=true');
		  }
        ?>        
    </head>
    <body onload="ocultarcalendario()">
        <?php include("./header.php"); ?>
        <div id="content">
            <div id="secondHeader">
                <h2>Editar contacto</h2>
            </div>
            <?php 
            if(isset($_GET['id'])) {
            	$contactId = $_GET['id'];
					$sql = "SELECT * FROM CONTACT WHERE ID_CONTACT = '$contactId'";
					$result = mysqli_query($con, $sql);
					if (mysqli_num_rows($result) > 0) {
					while($row = mysqli_fetch_assoc($result)) {
					echo "<form action='' method='post'>";
            		echo "<div id='contactInfo'>";
							echo "<img src='./img/icons/nombre.png' alt=''><input type='text' name='nombre' placeholder='nombre' value='".$row['NAME']."'><input type='text' name='apellidos' placeholder='apellidos' value='".$row['SURNAME']."'><div class='enter'></div>";
							echo "<img src='./img/icons/telefono.png' alt=''><input type='text' name='telefono' placeholder='telefono' value='".$row['TELF']."'><div class='enter'></div>";
							echo "<img src='./img/icons/fec_nac.png' alt=''><input onClick='mostrarcalendario()' type='text' name='fec_nac' placeholder='fecha nacimiento' value='".$row['BIRTHDATE']."'><div class='enter'></div>";    	
							echo "<iframe src='calendario.html' id='calendar' width='680px' height='220px' scrolling='no'></iframe>";
            		echo "</div>"; 
            		echo "<div id='contactButton'>";
                		echo "<input class='return' Type='button' onClick='history.go(-1);return true;' value='Volver'>";
                		echo "<input class='guardar' type='submit' name='submit' value='Guardar'>";
            		echo "</div>"; 
            	echo "</form>";	
					}
					}
				}
            ?>
        </div>
        <div class="enter"></div>
        <?php include("./footer.php"); ?>
    </body>
</html>
