# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Gracias a este proyecto el repositorio crea una web con funcionalidades de agenda.
* Podras añadir contactos, editarlos, borrarlos y hacer filtro para buscarlos rápidamente.

### How do I get set up? ###

* Para que funcione necesitarás un servidor apache.
* La configuración del proyecto es muy sencilla.
* Instalar un servidor apache.
* Clonar el repositorio en carpeta local.
* En la carpeta archivos está el Script para generar una base de datos de prueba.
* El comando mysql -u "user" -p "password" (donde user es el usuario de mysql y password la contraseña).
* Copiarás y ejecutarás todas las lineas del fichero de creación de bbdd ubicado en archivos/createdb.sql
* Configurar el archivo conexión.php con las variables globales de tu mysql.
* Si ya tienes una bbdd, eliminas la linea create database Agenda, y en la linea Use agenda, cambiarás Agenda por el nombre de tu bbdd.

### Who do I talk to? ###

* Nicolas Zabala a15niczabigl@iam.cat
