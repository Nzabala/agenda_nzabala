<html>
    <head>
        <title>Agenda</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./css/agenda.css" />
        <script type='text/javascript' src='./js/funciones.js'></script>
        <?php 
        session_start();
		  include "conexion.php";
		  if($_SESSION['ID_USER'] == null) {
        	header("location: home.php");
        }
		  $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_NAME);
		  if (mysqli_error($con)){
				echo "<p>Error: ".mysqli_error($con).".</p>";
		  }
		  $limite = 4;
        ?>
    </head>
    <body onload="lanzadera();">
        <?php include("./header.php"); ?>
        <div id="content">
            <div id="secondHeader">
            <form action="" method="post">
                <h2>Mis Contactos</h2>
                <input type="text" name="buscar" placeholder="Buscar..." id="search"/>
                <input type="image" name="buscar" class="icons" src="./img/icons/search.png"/>
            </form>
            	 <input id="filter" type="image" name="filtrar" class="icons" src="./img/icons/filter.png" onclick="iframeFiltro()"/>
                <input id="addcontact" type="image" name="addcontact" class="icons" src="./img/icons/plus.png" onclick="location.href='./addcontact.php'"/>
                            
            </div>
            <div id="contacts">
            <div id='promtB'><iframe id="iframeB" width='100%' height='100%'></iframe></div>
            <div id='promtC'><iframe id="iframeC" width='100%' height='100%'></iframe></div>
            <div id='promtF'><iframe id="iframeF" width='100%' height='100%'></iframe></div>
            <?php
            $userid = $_SESSION["ID_USER"];
				$sql = "SELECT * FROM CONTACT WHERE ID_USER_FK = '$userid'";
				$result = mysqli_query($con, $sql);
				$nfilas = mysqli_num_rows($result);
				$npaginas = $nfilas / $limite;
				if(isset($_GET["page"])){
					if($_GET["page"] == "fin"){
						$offset = $npaginas * $limite - $limite;
						$sql = "SELECT * FROM CONTACT WHERE ID_USER_FK = '$userid' LIMIT $limite OFFSET $offset";				
					} 
					elseif(($_GET["page"] == 0) || ($_GET["page"] == "ini")) {
						$sql = "SELECT * FROM CONTACT WHERE ID_USER_FK = '$userid' LIMIT $limite";
					}else{
						$offset = $_GET["page"] * $limite;
						$sql = "SELECT * FROM CONTACT WHERE ID_USER_FK = '$userid' LIMIT $limite OFFSET $offset";
					}
            }else $sql = $sql . " LIMIT " . $limite;
            if(isset($_POST['buscar'])){
            	$busqueda = "%".$_POST["buscar"]."%";          	
            	$sql = "SELECT * FROM CONTACT WHERE ID_USER_FK = '$userid' AND (NAME LIKE '%".$busqueda."%') OR (SURNAME LIKE '%".$busqueda."%') OR (BIRTHDATE LIKE '%".$busqueda."%') OR (TELF LIKE '%".$busqueda."%')";
				}
				if(isset($_GET['filtro'])){
					$orden = $_GET["orden"];
					$campo = $_GET["campo"];
					$sql = "SELECT * FROM CONTACT WHERE ID_USER_FK = '$userid' ORDER BY $campo $orden";
				}

				$result = mysqli_query($con, $sql);
				if (mysqli_num_rows($result) > 0) {
						while($row = mysqli_fetch_assoc($result)) {
							$id = $row["ID_CONTACT"];
					
							echo "<div class='contactContent'>";							
							echo "<div class='contactHeader'>";
							echo "<a onclick='iframeContacto($id)'>" . $row["NAME"] . " " . $row["SURNAME"] . "</a>";
							echo "<a href='./edcontact.php?id=$id'><input type='image' class='icons' src='./img/icons/edit.png'/></a>";							
							echo "<input type='image' class='icons' src='./img/icons/eliminate.png' onclick='iframeBorrar($id)'/>";
							echo "</div>";
							echo "<div class='contactInfo'>";
							echo "<span class='info'><img src='./img/icons/telefono.png'/>" . $row["TELF"] . "</span></p>";
							echo "<span class='info'><img src='./img/icons/fec_nac.png'/>" . $row["BIRTHDATE"] . "</span></p>";
							echo "</div>";
							echo "</div>";
							 
						}
				}
            ?>
            <?php 
           		echo "</div>";
            	echo "</div>";
            	
            	echo "<div id='navigation'>";
               	echo "<ul class='pagination'>";
                  	echo "<li><a href='./agenda.php?page=ini'>«</a></li>";
                  	for($i = 0 ; $i < $npaginas ; $i++) {
                  		echo "<li><a id=".$i." href='./agenda.php?page=".$i."'>".$i."</a></li>";
                  	}
                  	echo "<li><a href='./agenda.php?page=fin'>»</a></li>";
                	echo "</ul><br>";
            	echo "</div>";
        			echo "</div>";
        			
        		?>
        <?php include("./footer.php"); ?>
        <script type="text/javascript">
        var variable;
        variable = "<?php echo $_GET['page']; ?>";
        		if ((variable < 100) && (variable >=0)){
					document.getElementById(variable).setAttribute("class","active");
        		}
       
        </script>
    </body>
</html>
