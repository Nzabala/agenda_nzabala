function construirfecha(){
      fecha = new Date();
      mes = fecha.getMonth();
      anno = fecha.getFullYear();
      mesS = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
}

function calendario() {
      //fecha = new Date(); lo cogerá de la funcion que se carga en ONload al entrar en el iframe de calendario, obteniendo en una variable global fecha
      var ano = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      var diaSemana = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];
      //anno = fecha.getFullYear(); 
      if ((anno % 4 && anno % 400) === 0) {
            ano[1] = 29;
      }
      //mes = fecha.getMonth();

      var dias = ano[mes];
      var n = 1;
      
      var fechaObjeto = new Date(anno, mes);
      
      var fechaString = fechaObjeto.toDateString();
      var fechaDia = fechaString.slice(0, 3);
      var diaEmpieza = 0;
      switch (fechaDia) {
            case 'Mon':
                  fechaDia = "Lunes";
                  diaEmpieza = 0;
                  break;
            case 'Tue':
                  fechaDia = "Martes";
                  diaEmpieza = 1;
                  break;
            case 'Wed':
                  fechaDia = "Miercoles";
                  diaEmpieza = 2;
                  break;
            case 'Thu':
                  fechaDia = "Jueves";
                  diaEmpieza = 3;
                  break;
            case 'Fri':
                  fechaDia = "Viernes";
                  diaEmpieza = 4;
                  break;
            case 'Sat':
                  fechaDia = "Sabado";
                  diaEmpieza = 5;
                  break;
            case 'Sun':
                  fechaDia = "Domingo";
                  diaEmpieza = 6;
                  break;
      }
      
      var divCalendario = document.createElement("div"); //creo el contenedor calendario
      document.getElementById("calendar").appendChild(divCalendario); //lo enganchamos como hijo en el ID "demo2"
      divCalendario.setAttribute("class", "calendario"); //Agregamos el atributo
      divCalendario.setAttribute("id", "calendario"); //agregaremos el id necesario para matarlo y recrearlo cuando se suba y baje el mes
      
      var divMes = document.createElement("div");
      var divAnno = document.createElement("div");
      divMes.setAttribute("class","mes");
      divAnno.setAttribute("class","anno");
      var mesString = document.createTextNode(mesS[mes]);
      divMes.appendChild(mesString);
      var annoString = document.createTextNode(anno);
      divAnno.appendChild(annoString);
      divCalendario.appendChild(divMes);
      divCalendario.appendChild(divAnno);

      var divDiaSemana = document.createElement("div"); //creo el contenedor de la semana por dias
      divCalendario.appendChild(divDiaSemana); //lo enganchamos como hijo del calendario
      divDiaSemana.setAttribute("class", "diasemana"); //agregamos atributo

      for (var k = 0; k < diaSemana.length; k++) {
            var divTextoDia = document.createElement("div");
            var textoDia = document.createTextNode(diaSemana[k]);
            divDiaSemana.appendChild(divTextoDia);
            divTextoDia.setAttribute("class", "textodia");
            divTextoDia.appendChild(textoDia);
      }

      var dia;

      for (var i = 0; i <= 5; i++) {
            var divSemana = document.createElement("div");
            divSemana.setAttribute("class", "semana");
            divCalendario.appendChild(divSemana);
            for (var j = 0;(j < 7) && (n <= dias); j++) { //obtendremos el número del dia en que estamos para saber que dia empieza el mes y conseguir que el calendario empiece en el dia correcto.
                  if ((i === 0) && (j < diaEmpieza)) {
                        dia = document.createTextNode("-");
                        n = 0;
                  } else {
                        dia = document.createTextNode(n);
                  }
                  var divDia = document.createElement("div");
                  var enlace = document.createElement("a");
                  enlace.setAttribute("onClick", "addfecha("+n+","+mes+","+anno+")");
                  divDia.setAttribute("class", "dia");
                  selDivSemana = document.getElementsByClassName("semana");
                  selDivSemana[i].appendChild(divDia);
                  divDia.appendChild(enlace);
                  enlace.appendChild(dia);
                  n++;
            }

      }
}

//esta funcion la llama el onload para que esté oculto el calendario y cuando acabamos de seleccionar un datePicker

function mostrarcalendario(){
      parent.document.getElementById("calendar").style.display = "block";
}

function ocultarcalendario(){
      parent.document.getElementById("calendar").style.display = "none";
}

function addfecha(n, mes, anno) {
      var input = parent.document.getElementsByName("fec_nac");
      mes = mes + 1;
      //para que las fechas queden siempre igual
      if (n<10) {
			n= "0"+n;      	
     	}
     	if (mes<10) {
			mes= "0"+mes;     	
     	}
      var fechasel = anno+"/"+mes+"/"+n;
      input[0].value = fechasel;
      ocultarcalendario();
}

function bajarmes(){
      var aux = document.getElementById("calendario");
      aux.parentNode.removeChild(aux);
      mes = mes -1;
      if (mes < 0){
            anno = anno - 1;
            mes = 11;
            calendario();
      }else{
            calendario();
      }
}
function subirmes(){
      var aux = document.getElementById("calendario");
      aux.parentNode.removeChild(aux);
      mes = mes + 1;
      if (mes >= 11){
            anno = anno + 1;
            mes = 0;
            calendario();
      }else{
            calendario();
      }
      
}
function subiranno(){
      var aux = document.getElementById("calendario");
      aux.parentNode.removeChild(aux);
      anno = anno +1;
      calendario();
}
function bajaranno(){
      var aux = document.getElementById("calendario");
      aux.parentNode.removeChild(aux);
      anno = anno -1;
      calendario();
}
function validarForm() {
      if(!validarFecha()){
      	return false;
      }else return true;
}

function validarFecha() {
	var x = document.forms["addcontact"]["fec_nac"].value;
   var year = x.substr(0,4);
   var month = x.substr(5,2);
   month--;
   var day = x.substr(8,2);
   var hoy = new Date();
   var fecha = new Date(year,month,day);
   if (fecha>hoy) {
		alert("Fecha incorrecta");
      var input = document.getElementsByName("fec_nac");
    	input[0].value = "";		
		return false;
	}else return true; 	  
}

function validarTelf() {
	var x = document.forms["addcontact"]["telefono"].value;
	var patron = /^\d{9}$/;
  	if(!(patron.test(x))){
  		return false;
	}else return true;
}